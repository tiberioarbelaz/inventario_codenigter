<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Language: Italian
 * Module: Quotations
 *
 */

$lang['add_quote']          = 'Aggiungere Quotazione';
$lang['edit_quote']         = 'Modifica Quotazione';
$lang['delete_quote']       = 'Elimina Quotazione';
$lang['delete_quotes']      = 'Elimina Quotazioni';
$lang['quote_added']        = 'Quotazione correttamente aggiunta';
$lang['quote_updated']      = 'Quotazione correttamente aggiornata';
$lang['quote_deleted']      = 'Quotazione correttamente eliminata';
$lang['quotes_deleted']     = 'Quotazioni correttamente eliminate';
$lang['quote_details']      = 'Dettagli Quotazione';
$lang['email_quote']        = 'Email Quotazione';
$lang['view_quote_details'] = 'Vedi Dettagli Quotazione';
$lang['quote_no']           = 'N° Quotazione';
$lang['send_email']         = 'Invia Email';
$lang['quote_items']        = 'Oggetti Quotazione';
$lang['no_quote_selected']  = 'Nessuna quotazione selezionata. Seleziona almeno una quotazione.';
$lang['create_sale']        = 'Crea Vendita';
$lang['create_purchase']    = 'Crea Acquisto';
$lang['create_invoice']     = 'Crea Vendita';
