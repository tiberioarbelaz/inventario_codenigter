<?php

defined('BASEPATH') or exit('No direct script access allowed');



$lang['shop_name']            = 'Nombre de la Tienda';
$lang['page']                 = 'Pagina';
$lang['pages']                = 'Paginas';
$lang['slug']                 = 'Slug';
$lang['order']                = 'Orden';
$lang['menu_order']           = 'Menu Orden';
$lang['body']                 = 'Body';
$lang['edit_page']            = 'Editar Pagina';
$lang['delete_page']          = 'Eliminar Pagina';
$lang['delete_pages']         = 'Eliminar Paginas';
$lang['page_added']           = 'Page successfully added';
$lang['page_updated']         = 'Page successfully updated';
$lang['page_deleted']         = 'Page successfully deleted';
$lang['pages_deleted']        = 'Pages successfully deleted';
$lang['order_no']             = 'Orden No.';
$lang['title_required']       = 'Titulo requerido';
$lang['slug_required']        = 'Slug es requerido';
$lang['description_required'] = 'la Descripcion es requerida';
$lang['body_required']        = 'Body es requerido';
$lang['about_link']           = 'Pagina de Nosotros';
$lang['terms_link']           = 'Pagina de terminos y condiciones';
$lang['privacy_link']         = 'Pagina Politicas de privacidad';
$lang['contact_link']         = 'Pagina de contacto';
$lang['cookie_link']          = 'Pagina de cookies';
$lang['cookie_message']       = 'Cookie de Mensaje';
$lang['payment_text']         = 'Texto de pago';
$lang['follow_text']          = 'Follow Text';
$lang['facebook']             = 'Facebook';
$lang['twitter']              = 'Twitter';
$lang['google_plus']          = 'Google Plus';
$lang['instagram']            = 'Instgram';
$lang['image']                = 'Image';
$lang['link']                 = 'Link';
$lang['caption']              = 'Caption';
$lang['show_in_top_menu']     = 'Mostrar en los menús superiores';
$lang['products_page']        = 'Pagina de productos (grid style)';
$lang['leave_gap']            = 'Dejar espacio debajo';
$lang['re_arrange']           = 'Reorganizar productos';
