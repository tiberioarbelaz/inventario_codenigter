<?php

defined('BASEPATH') or exit('No direct script access allowed');



$lang['edit_return']                        = 'تعديل مرتجع';
$lang['delete_return']                      = 'حذف مرتجع';
$lang['return_sale']                        = 'بيع مرتجع';
$lang['return_note']                        = 'ملاحظات المرتجع';
$lang['staff_note']                         = 'ملاحظات الموظف';
$lang['you_will_loss_return_data']          = 'سوف تفقد بيانات المرتجع الحالية';
$lang['delete_returns']                     = 'حذف المرتجعات';
$lang['staff_note']                         = 'ملاحظات الموظف';
$lang['return_added']                       = 'تمت اضافة المرتجع بنجاح';
$lang['return_updated']                     = 'تمت تعديل المرتجع بنجاح';
$lang['return_deleted']                     = 'تم حذف المرتجع بنجاح';
$lang['return_x_edited_older_than_x_days']  = 'لايمكن تعديل مرتجع اقدم من %d يوم او ايام';
