<?php

defined('BASEPATH') or exit('No direct script access allowed');



$lang['sale_added']       = 'عزيزي {customer}, طلبك رقم  (Ref. {sale_reference}) قد تم استلامه, نرجوا اتمام عملية الدفع بمبلغ  {grand_total}. شكرا';
$lang['payment_received'] = 'عزيزي {customer}, عملية الدفع رقم  (Ref. {payment_reference}, بمبلغ {amount}) قد تم استلامها, سيتم اكملاء اجراءات طلبك. شكرا';
$lang['delivering']       = 'عزيزي {customer}, جاري شحن طلبكم رقم  (Ref. {delivery_reference}).';
$lang['delived']          = 'عزيزي {customer}, طلبكم رقم  (Ref. {sale_reference}) قد تم استلامه بواسطة {received_by}.';
